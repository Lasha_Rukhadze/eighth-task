package com.example.eighthtask

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.eighthtask.databinding.Items3Binding

class RecyclerAdapter2 (private val figures : MutableList<ItemModelSecond>, private val itemsListener : ItemsInterface) : RecyclerView.Adapter<RecyclerAdapter2.FiguresViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int):  FiguresViewHolder {
        val itemView = Items3Binding.inflate(LayoutInflater.from(parent.context), parent, false)
        return FiguresViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: FiguresViewHolder, position: Int) {
        holder.bind()
    }

    override fun getItemCount(): Int {
        return figures.size
    }

    inner class FiguresViewHolder(private val binding : Items3Binding) : RecyclerView.ViewHolder(binding.root), View.OnLongClickListener {
        lateinit var model : ItemModelSecond
        fun bind() {
            model = figures[adapterPosition]
            binding.figureName.text = model.figureName
            binding.figure.setImageResource(model.figure)
            binding.root.setOnLongClickListener(this)
        }

        override fun onLongClick(v: View?): Boolean {
                itemsListener.userItemOnClick(adapterPosition)
                return true
        }
    }
}