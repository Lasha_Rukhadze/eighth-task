package com.example.eighthtask

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.eighthtask.databinding.Items1Binding
import com.example.eighthtask.databinding.Items2Binding

class RecyclerAdapter (private val letters: MutableList<ItemModelFirst>, private val itemsListener : ItemsInterface) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object {
        private const val GREEK_LETTERS = 1
        private const val LATIN_LETTERS = 2
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (viewType == GREEK_LETTERS){
            val itemView1 = Items1Binding.inflate(LayoutInflater.from(parent.context), parent, false)
                return GreekLetterViewHolder(itemView1)
        }
        else {
            val itemView2 = Items2Binding.inflate(LayoutInflater.from(parent.context),parent, false)
                return LatinLetterViewHolder(itemView2)
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when(holder){
            is LatinLetterViewHolder -> holder.bind()
            is GreekLetterViewHolder -> holder.bind()
        }
    }

    override fun getItemCount(): Int {
        return letters.size
    }

    inner class LatinLetterViewHolder(private val binding: Items2Binding) : RecyclerView.ViewHolder(binding.root), View.OnLongClickListener{
        private lateinit var modelFirst : ItemModelFirst
        fun bind(){
            modelFirst = letters[adapterPosition]
            binding.latinName.text = modelFirst.name
            binding.root.setOnLongClickListener(this)
        }

        override fun onLongClick(v: View?): Boolean {
            itemsListener.userItemOnClick(adapterPosition)
            return true
        }
    }

    inner class GreekLetterViewHolder(private val binding : Items1Binding) : RecyclerView.ViewHolder(binding.root), View.OnLongClickListener {
        private lateinit var modelFirst: ItemModelFirst
        fun bind() {
            modelFirst = letters[adapterPosition]
            binding.greekName.text = modelFirst.name
            binding.letter.setImageResource(modelFirst.picture ?: R.drawable.ic_alpha)
            binding.root.setOnLongClickListener(this)
        }

        override fun onLongClick(v: View?): Boolean {
            itemsListener.userItemOnClick(adapterPosition)
            return true
        }
    }

    override fun getItemViewType(position: Int): Int {
        val model = letters[position]
        return if (model.picture != null)
            GREEK_LETTERS
        else{
            LATIN_LETTERS
        }
    }
}