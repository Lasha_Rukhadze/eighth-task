package com.example.eighthtask

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.eighthtask.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    lateinit var binding: ActivityMainBinding
    private lateinit var adapter1 : RecyclerAdapter
    private lateinit var adapter2 : RecyclerAdapter2
    private var letters = mutableListOf<ItemModelFirst>()
    private var figures = mutableListOf<ItemModelSecond>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        initializer()
    }

    private fun initializer(){
        setData()
        adapter1 = RecyclerAdapter(letters, object : ItemsInterface{
            override fun userItemOnClick(position: Int) {
                letters.removeAt(position)
                adapter1.notifyItemRemoved(position)
            }
        })
        binding.recycler1.layoutManager = GridLayoutManager(this, 2)
        binding.recycler1.adapter = adapter1
        adapter2 = RecyclerAdapter2(figures, object : ItemsInterface{
            override fun userItemOnClick(position: Int) {
                figures.removeAt(position)
                adapter2.notifyItemRemoved(position)
            }
        })
        binding.recycler2.layoutManager = GridLayoutManager(this, 2)
        binding.recycler2.adapter = adapter2
    }

    private fun setData(){
        letters.add(ItemModelFirst("Alpha", R.drawable.ic_alpha))
        letters.add(ItemModelFirst("Beta", R.drawable.ic_beta))
        letters.add(ItemModelFirst("Y"))
        letters.add(ItemModelFirst("Delta", R.drawable.ic_delta))
        letters.add(ItemModelFirst("Epsilon", R.drawable.ic_epsilon))
        letters.add(ItemModelFirst("A"))
        letters.add(ItemModelFirst("Gamma", R.drawable.ic_gamma))
        letters.add(ItemModelFirst("Dseta", R.drawable.ic_dseta))
        letters.add(ItemModelFirst("N"))
        letters.add(ItemModelFirst("K"))
        letters.add(ItemModelFirst("Ji", R.drawable.ic_ji))
        letters.add(ItemModelFirst("Rho", R.drawable.ic_rho))
        letters.add(ItemModelFirst("E"))
        letters.add(ItemModelFirst("Tau", R.drawable.ic_tau))
        figures.add(ItemModelSecond(R.drawable.ic_achilles, "Achilles"))
        figures.add(ItemModelSecond(R.drawable.ic_trojan, "Trojan"))
        figures.add(ItemModelSecond(R.drawable.ic_helmet__1_, "Spartan Helmet"))
        figures.add(ItemModelSecond(R.drawable.ic_torch, "Greek Torch"))
        figures.add(ItemModelSecond(R.drawable.ic_juno, "Juno"))
        figures.add(ItemModelSecond(R.drawable.ic_parthenon, "Parthenon"))
        figures.add(ItemModelSecond(R.drawable.ic_jupiter, "Jupiter"))
        figures.add(ItemModelSecond(R.drawable.ic_philosopher, "Socrates"))
        figures.add(ItemModelSecond(R.drawable.ic_achilles, "Achilles"))
        figures.add(ItemModelSecond(R.drawable.ic_trojan, "Trojan"))
        figures.add(ItemModelSecond(R.drawable.ic_helmet__1_, "Spartan Helmet"))
        figures.add(ItemModelSecond(R.drawable.ic_torch, "Greek Torch"))
        figures.add(ItemModelSecond(R.drawable.ic_juno, "Juno"))
        figures.add(ItemModelSecond(R.drawable.ic_parthenon, "Parthenon"))
        figures.add(ItemModelSecond(R.drawable.ic_jupiter, "Jupiter"))
        figures.add(ItemModelSecond(R.drawable.ic_philosopher, "Socrates"))
        figures.add(ItemModelSecond(R.drawable.ic_achilles, "Achilles"))
        figures.add(ItemModelSecond(R.drawable.ic_trojan, "Trojan"))
        figures.add(ItemModelSecond(R.drawable.ic_helmet__1_, "Spartan Helmet"))
        figures.add(ItemModelSecond(R.drawable.ic_torch, "Greek Torch"))
        figures.add(ItemModelSecond(R.drawable.ic_juno, "Juno"))
        figures.add(ItemModelSecond(R.drawable.ic_parthenon, "Parthenon"))
        figures.add(ItemModelSecond(R.drawable.ic_jupiter, "Jupiter"))
        figures.add(ItemModelSecond(R.drawable.ic_philosopher, "Socrates"))
    }
}